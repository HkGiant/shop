<?php 
        
        $rand = $db->query('SELECT * FROM products WHERE rating >= 4 ORDER BY RAND() LIMIT 2')->fetch();
 ?>

<div class="product">
                    <div class="thumbnail">
                        <img src="<?= $rand['picture'] ?>" alt="missing pict">
                        <div class="caption">
                            <h4 class="pull-right"><?= $rand['price'] ?>€</h4>
                            <h4><a href=""><?= $rand['name'] ?></a>
                            </h4>
                            <p><?= $rand['description'] ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right"><?= $rand['rating'] ?></p>
                            <p>
                                <?php
                                   for ($i=1; $i <= $rand['rating']; $i++) { ?>
                                   <span class="glyphicon glyphicon-star"></span>
                                <?php } ?>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href="product.php?id=<?= $rand['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href="product.php?id=<?= $rand['id'] ?>"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->