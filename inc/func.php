<?php
function cutString($text, $max_length = 0, $end = '...', $sep = '[|]') {
	if ($max_length > 0 && strlen($text) > $max_length) {
		
		$text = wordwrap($text, $max_length, $sep, true);

		$text =explode($sep, $text);

		return $text[0].$end;
	}
	return $text;
}

?>