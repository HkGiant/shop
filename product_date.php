 <?php
       include 'inc/func.php';
       
       $prod_recents =$db->query('SELECT * FROM products ORDER BY date DESC LIMIT 6')->fetchAll();

        foreach ($prod_recents as $key => $prod_recent) {

 ?>

 <div class="product col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?= $prod_recent['picture'] ?>" alt="missing pict">
                            <div class="caption">
                                <h4 class="pull-right"><?= $prod_recent['price'] ?>€</h4>
                                <h4><a href="#"><?= $prod_recent['name'] ?></a>
                                </h4>
                                <p><?= cutString(nl2br($prod_recent['description']),100, ' ...', '|') ?>
                                </p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right"><?= $prod_recent['rating'] ?></p>
                                <p>
                                <?php
                                   for ($i=1; $i <= $prod_recent['rating']; $i++) { ?>
                                   <span class="glyphicon glyphicon-star"></span>
                                <?php } ?>
                                    
                                </p>
                            </div>
                            <div class="btns clearfix">
                                <a class="btn btn-info pull-left" href="product.php?id=<?= $prod_recent['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                <a class="btn btn-primary pull-right" href="product.php?id=<?= $prod_recent['id'] ?>"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                            </div>
                        </div><!-- /.thumbnail -->
                    </div><!-- /.product -->

<?php } ?> 