<?php
       include 'inc/func.php';

       $search_results =$db->query('SELECT * FROM products WHERE name /*LIKE :search*/ ')->fetchall();

        foreach ($search_results as $key => $search_result) {
        

?>

 

 <div class="product col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?= $search_results['picture'] ?>" alt="missing pict">
                            <div class="caption">
                                <h4 class="pull-right"><?= $search_result['price'] ?>€</h4>
                                <h4><a href="#"><?= $search_result['name'] ?></a>
                                </h4>
                                <p><?= cutString(nl2br($search_result['description']),100, ' ...', '|') ?>
                                </p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right"><?= $search_result['rating'] ?></p>
                                <p>
                                <?php
                                   for ($i=1; $i <= $search_result['rating']; $i++) { ?>
                                   <span class="glyphicon glyphicon-star"></span>
                                <?php } ?>
                                    
                                </p>
                            </div>
                            <div class="btns clearfix">
                                <a class="btn btn-info pull-left" href="product.php?id=<?= $search_result['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                <a class="btn btn-primary pull-right" href="product.php?id=<?= $search_result['id'] ?>"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                            </div>
                        </div><!-- /.thumbnail -->
</div><!-- /.product -->

<?php } ?>